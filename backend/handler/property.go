package handler

import (
	"fmt"
	"net/http"
	"properqies/properties"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
)

type propertyHandler struct {
	propertyService properties.Service
}

func PropertyHandler(propertyService properties.Service) *propertyHandler{
	return &propertyHandler{propertyService}
}

func (handler *propertyHandler) GetAllPropertyHandler(c *gin.Context) {
	property, err := handler.propertyService.FindAll()
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"errors": err,
		})
	}

	c.JSON(http.StatusOK, gin.H{
		"data": property,
	})
}

func (handler *propertyHandler) GetOneProperty(c *gin.Context) {
	idString := c.Param("id")
	id, _ := strconv.Atoi(idString)
	property, err := handler.propertyService.FindById(id)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"errors": err,
		})

		return
	}

	if property.ID == 0 {
		c.JSON(http.StatusNotFound, gin.H{
			"errors": "Data not found!",
		})

		return
	}
	
	c.JSON(http.StatusOK, gin.H{
		"data": property,
	})
}

func (handler *propertyHandler) PostPropertyHandler(c *gin.Context) {
	var propertyRequest properties.PropertyRequest

	err := c.ShouldBindJSON(&propertyRequest)

	if err != nil {
		errorMessages := []string{}
		for _, e := range err.(validator.ValidationErrors) {
			errorMessage := fmt.Sprintf("Error on field %s, condition %s", e.Field(), e.ActualTag())
			errorMessages = append(errorMessages, errorMessage)
		}
 
		c.JSON(http.StatusBadRequest, gin.H{
			"error": errorMessages,
		})
	}

	property, err := handler.propertyService.Create(propertyRequest)
	if  err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"errors": err,
		})
	}
	
	c.JSON(http.StatusOK, gin.H{
		"data": property,
	})

}