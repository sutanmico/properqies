package properties

import "gorm.io/gorm"

type Repository interface {
	FindAll() ([]Property, error)
	FindById(ID int) (Property, error)
	Create(property Property) (Property, error)
	Update(ID int) (Property, error)
	Delete(ID int) (Property, error)
}

type repository struct {
	db *gorm.DB
}

func PropertyRepository(db *gorm.DB) *repository {
	return &repository{db}
}

func (r *repository) FindAll() ([]Property, error) {
	var property []Property
	err := r.db.Find(&property).Error
	return property, err
}

func (r *repository) FindById(ID int) (Property, error) {
	var property Property
	err := r.db.Find(&property, ID).Error
	return property, err
}

func (r *repository) Create(property Property) (Property, error) {
	err := r.db.Create(&property).Error
	return property, err
}

func (r *repository) Update(ID int) (Property, error) {
	var property Property
	err := r.db.Find(&property, ID).Error
	if err != nil {
		return property, err
	}

	return property, err

}

func (r *repository) Delete(ID int) (Property, error) {
	var property Property
	err := r.db.Find(&property, ID).Error
	if err != nil {
		return property, err
	}

	return property, err
}