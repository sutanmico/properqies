package properties

import "time"

type Property struct {
	ID          int
	Title       string
	Description string
	Price       int
	Status 		string
	CreatedAt   time.Time
	UpdatedAt   time.Time
}