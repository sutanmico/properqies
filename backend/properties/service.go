package properties

type Service interface {
	FindAll() ([]Property, error)
	FindById(ID int) (Property, error)
	Create(propertyRequest PropertyRequest) (Property, error)
	Update(ID int) (Property, error)
	Delete(ID int) (Property, error)
}

type service struct {
	repository Repository
}

func PropertyService(repository Repository) *service {
	return &service{repository}
}

func (s *service) FindAll() ([]Property, error) {
	return s.repository.FindAll()
}

func (s *service) FindById(ID int) (Property, error) {
	return s.repository.FindById(ID)
}

func (s *service) Create(propertyRequest PropertyRequest) (Property, error) {
	price, _ := propertyRequest.Price.Int64()

	property := Property{
		Title:       propertyRequest.Title,
		Description: propertyRequest.Description,
		Price:       int(price),
		Status:      propertyRequest.Status,
	}

	return s.repository.Create(property)
}

func (s *service) Update(ID int) (Property, error) {
	return s.repository.Update(ID)
}

func (s *service) Delete(ID int) (Property, error) {
	return s.repository.Delete(ID)
}