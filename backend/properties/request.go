package properties

import "encoding/json"

type PropertyRequest struct {
	Title       string      `json:"title" binding:"required"`
	Description string      `json:"description" binding:"required"`
	Price       json.Number `json:"price" binding:"required,number"`
	Status      string 		`json:"status" binding:"required"`
}