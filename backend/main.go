package main

import (
	"log"
	"properqies/handler"
	"properqies/properties"

	"github.com/gin-gonic/gin"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func main() {
	 dsn := "root:@tcp(127.0.0.1:3306)/properqies?charset=utf8mb4&parseTime=True&loc=Local"
	 db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	 if  err != nil {
		log.Fatal("Connection database error")
	 }

	db.AutoMigrate(&properties.Property{})

	propertyRepository := properties.PropertyRepository((db))
	propertyService := properties.PropertyService(propertyRepository)
	propertyHandler := handler.PropertyHandler(propertyService)

	router := gin.Default()
	
	v1 := router.Group("/v1")

	v1.GET("/properties", propertyHandler.GetAllPropertyHandler)	
	v1.GET("/property/:id", propertyHandler.GetOneProperty)	
	v1.POST("/property", propertyHandler.PostPropertyHandler)	

	router.Run()
}